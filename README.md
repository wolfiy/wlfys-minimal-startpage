# Startpage
A simple, minimal, and easy to modify, startpage. With built-in support for dark and light themes with multiple images.

![Dark theme](assets/demo/dark.png)
![Light theme](assets/demo/light.png)

A live demo is available [here](https://wolfiy.gitlab.io/wlfys-minimal-startpage/).


## Credits
I was inspired by [RamenMaestro's startpage](https://github.com/RamenMaestro/startpage).

The art is by [Arkestar](https://twitter.com/Arkestar).